public class Main {

    public static void main (String[] args)
    {
        System.out.println("Hello");

        //Krieger erstellt ein Objekt namens krieger dieses hält dann z.B den Namen den man ihm gibt und die Lebenspunkte
        Krieger krieger = new Krieger(100, "Talon");
        krieger.setSchwert(new Schwert());
        System.out.println("Der Krieger heißt: " + krieger.getName() + " und hat " + krieger.getLebenspunkte() + " Lebenspunkte ");
        Magier magier = new Magier(100, "Torlof");
        System.out.println("Der Magier heißt: " + magier.getName() + " und hat " + magier.getLebenspunkte() + " Lebenspunkte");

        System.out.println(krieger.getName() + " und " + magier.getName() + " kämpfen jetzt!");

        krieger.kriegerHautDrauf();
        magier.setStab(new Stab());

        // Klassenaufruf "Kampf" erstellt Obejekt Kampf das 2 andere Methoden bzw Obejekte hält.
        //mit denen man dann weiteres machen kann
        Kampf kampf = new Kampf(krieger,magier);
        //kampf.kampfMagier.getLebenspunkte();
        System.out.println("Magier seine Lebenspunkte " + kampf.kampfMagier.getLebenspunkte());
        kampf.macheSchaden();
        //System.out.println(magier.lebenspunkte);






    }

}

