public class Stab {

    protected String name;
    protected String farbe;
    protected int schaden = 10;


    public Stab()
    {
        this.farbe = "unbekannt";
        this.name = "unbekannt";
    }

    public void hauDrauf()
    {
        this.schaden = 10;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFarbe(String farbe) {
        this.farbe = farbe;
    }

    public int getSchaden() {
        return schaden;
    }
}


