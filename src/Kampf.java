import java.util.Random;

public class Kampf {
    //protected heißt das er auf seine eigene Variablen zugreift so das man sie immer verwenden kann auch andere für sich selbst!
    // "Krieger kampfKrieger" heißt quasi das die Klasse Krieger in eine Variable kampfKrieger gepackt wird!
    // so kann man auf die Methoden und Objekte innerhalb der Klasse Krieger oder Magier zugreifen.
    protected Krieger kampfKrieger;
    protected Magier kampfMagier;
    protected Random random = new Random();

    public Kampf(Krieger kampfKrieger, Magier kampfMagier) {
        this.kampfKrieger = kampfKrieger;
        this.kampfMagier = kampfMagier;

    }

    public void macheSchaden() {
        int count = 0;
        while (this.kampfMagier.getLebenspunkte() > 0 && this.kampfKrieger.getLebenspunkte() > 0)
        {
            int random = this.random.nextInt(10);
            /***
             * "Debugger"
             * System.out.println("Magier: " + this.kampfMagier.getLebenspunkte());
            System.out.println("Krieger: " + this.kampfKrieger.getLebenspunkte());
            System.out.println("Random: " + random);
            ***/
            if (random >= 5)
            {
                int kriegerSchaden = this.kampfKrieger.schwert.getSchaden();
                this.kampfMagier.veringereLeben(kriegerSchaden);
                System.out.println("Krieger " + this.kampfKrieger.getName() + " macht " + kriegerSchaden + " an " + this.kampfMagier.getName() + "\n");
                System.out.println("Magier Lebensstand: " + this.kampfMagier.getLebenspunkte());

            }
            else
            {
                System.out.println("Daneben!");
            }

            if (random <= 5)
            {
                int magierSchaden = this.kampfMagier.stab.getSchaden();
                this.kampfKrieger.veringereLeben(magierSchaden);
                System.out.println("Magier " + this.kampfMagier.getName() + " macht " + magierSchaden + " an " + this.kampfKrieger.getName() + "\n");
                System.out.println("Krieger Lebensstand: " + this.kampfKrieger.getLebenspunkte());
            }
            else
            {
                System.out.println("Daneben!");
            }
            count++;
            if(count>20)
            {
                break;
            }

        }
        if (this.kampfMagier.getLebenspunkte() == 0)
        {
            System.out.println("Der Magier " + kampfMagier.getName() + " ist tot!\n" + " Der Krieger " + kampfKrieger.getName() + " hat " + this.kampfKrieger.getLebenspunkte() + " Lebenspunkte");
        }
        if (this.kampfKrieger.getLebenspunkte() == 0)
        {
            System.out.println("Der Krieger " + kampfKrieger.getName() + " ist tot!\n" + " Der Magier " + kampfMagier.getName() + " hat " + this.kampfMagier.getLebenspunkte() + " Lebenspunkte");
        }
    }
}

/***
 * das war ein TEST :P
 */

/***
        while(this.kampfMagier.getLebenspunkte()>=0)

            if (this.random.nextInt(10) >= 5) {

                int kriegerSchaden = this.kampfKrieger.schwert.getSchaden();
                this.kampfMagier.veringereLeben(kriegerSchaden);
                System.out.println("Krieger " + this.kampfKrieger.getName() + " macht " + kriegerSchaden + " an " + this.kampfMagier.getName() + "\n");
                System.out.println(this.kampfMagier.getLebenspunkte());
                }

            else
                {
                    System.out.println("Daneben!\n");
                }

            if (this.kampfMagier.getLebenspunkte() <=0)
                {
                    System.out.println("Der Magier " + kampfMagier.getName() + " ist tot!");
                }

    }
}
***/
/***for(int i = 1; i <= 10; i++)
 {
 int kriegerSchaden = this.random.nextInt(10);
 this.kampfMagier.veringereLeben(kriegerSchaden);

 System.out.println("Krieger " + this.kampfKrieger.getName() + " macht " + kriegerSchaden + " an " + this.kampfMagier.getName() + "\n");

 if(this.kampfMagier.getLebenspunkte() <=0)
 {
 System.out.println("Der Magier " + kampfMagier.getName() + " ist tot!");
 }

 }
 ***/


